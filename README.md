# 2024-01-04, Version 5.2.0 (Current)

**Notable Changes**

* creating a new Envy Leveling Video, to provide a more realistical scenario








# SNS Knight 1h 1on1 Current State of Game
December 26, 2023

Utilizing the 1-on-1 EXP boost for monsters above level 141, I recommend leveling in Envy until 160. Ensure you have all DPS premiums, def upcuts, and electric def upcuts.

After trying the one-hour Envy leveling, reverting to open-world leveling won't be an option. The gained experience is too remarkable to settle for regular leveling.



# **Pets**
The Critical Chance Pet is a great choice for early levels up until you reach 140+ or 150. You can slowly make it stronger as you play the game. A Critical Chance Pet that's around 15-20% powerful is usually good enough. Pets can be expensive, so it's best to improve your pet slowly over time.


# **1h Knight SNS Build**
## **Skills**
### **Heart of Fury**
To provide a broad overview of its functioning:
10% of your overall FP transforms into supplementary damage.
With this understanding, it becomes evident that the FP-Necklace is indispensable in any scenario.

![Heart of Fury](hof.jpg)

Which means in the following:

Imagine we got a fresh lv 60 Knight....
Recorn set clean and **only** FP-Necklace +6 with lv. 57 Sparkling Sword unbuffed

if you go for 1on1 Leveling

| PURE STA  | PURE STR | 
| ------------- |:-------------:|
| HP: 6603    | HP: 3346    |
| FP: 2250    | FP: 1011    |
| ATK: **479**      | ATK: **1010**   |
| STR: 18     | STR: 136    |
| STA: 133    | STA: 15   |
| HOF:  **225** (10% of 2250)     | HoF: **101** (10% of 1011)   |


As evident, a PURE STR build (1010 ATK + 101, approximately equivalent to Demol +10) deals significantly more damage than a PURE STA build (479 ATK + 225, around a Demol +13). Despite the additional damage on paper from HoF with PURE STA, the base ATK of PURE STR is inherently stronger. With 1h scaling from STR, attacks become substantially more potent, particularly as scaling synergizes with crit rate and crit damage. The PURE STR build stands out as a superior choice for maximizing EXP % per hour.



For pure STA Knights, the Heart of Fury (HoF) serves as a beneficial tool for engaging in battles against giants, bosses, and superbosses such as CW and Meteonyker.

### **Rage**
![Rage](rage.jpg)


* **Attack +15%**
* **Incomming Damage +10%**
* **Increased -10%HP**
* **Block Penentration +15%**

An effective damage-dealing skill.
This skill is crucial for 1-on-1 encounters.


#**Gear**
## **Low Budget Build**
For the easiest starter build you can go is


* **Dretra +0 / Sparkling Sword +0**
* **Recorn Set +0 / Pangril +0**
* **Demol +6 / Vigor +6 / FP-Necklace +7 / Enduky +1**

I discourage opting for any critical build because it heavily depends on a consistently fast autoattack. This advice takes into account the buffs from FS/RM with only 50 INT.
You should get these following stats:

| Full STR  |
| ------------- |
| HP: 4452    | 
| FP: 1284   | 
| ATK: **1771**      | 
| STR: 173     | 
| STA: 41    | 
| Atkspd: 92    | 
| HOF:  **192** (15% of 1284)     | 


Based on HoF scaling, you should achieve approximately ~2k damage per autoattack. To make up for the lacking 8% Attack Speed, it's more efficient to utilize the Potion of Swiftness (a 10% attack speed potion). If you're averse to using attack speed potions, allocate points into Dexterity until you reach 100% Hitrate when buffed, although you may need to respec later. My preference leans towards maximizing damage.


## **High Budget Build**

With a investment you can build:

* **Dretra +8 - +10 / Historic Sword 1h +8 - +10**
* **(Lv. 30) Comet Set +8 16% ATK**
* **Demol +10 / Vigor +10 / FP-Necklace +7 / Enduky +1 / +2**
* **Critrate Pet**

you can similar expect to do this DMG:

https://youtu.be/jPs77KaR2Eg

this is just a session of me leveling at my 2nd SNS character 



# **Leveling Guide**
## **1-60**

A quick way to level up early on is to keep using the "Slash" skill you learn at level 15. You'll need something to refill your energy and basic gear (even the stuff you get from NPCs is okay). Use a weapon that's good for attacking (axe or sword), and focus on getting stronger. Once you reach level 30 and have a Comet Set (+6 to +8), just keep attacking normally.

Since some updated skills animation you can also use "Shield Bash" skill to fight yourself to lv 60.


## **60 - 74**

After changing jobs, focus on improving your sword skills: **Empowered Weapon**, **Blazing Sword**, and **Sword Mastery**. While swords are faster, axes hit harder. You can choose which one you like best.

When you reach level 60, you’ll learn a strong skill called Heart of Fury. It uses energy and is best used with high strength build(Full STR). You won’t need to refill energy as often as before, if you buy an enduky or fp necklace+7.

To level up quickly from levels 72 to 77, hunt Carrier Bombs. You can make a lot of money and lv's doing this. Create your own pace to level up. You can slowly grind penya here to buy the next level gear.It takes a bit of time, but it’s worth it for better gear.



## **LV 75**

Once you reach a higher level, get a Historic Sword. The better the sword, the stronger you'll be. You should also try to find an item called Enducky. An Enducky+1 is good enough; don’t spend too much on a +3 one. The price of items can change based on the game server.





## **LV 90**

Get the Wees/Weeshian Set and make it as strong as you can, but don’t spend too much money going past +8. If you have extra money, improve your Demols and Vigors to about  +9 or +10.

It gets harder to level up after level 90. But it’s easier now because of a new update.

## **LV 105**

The best gear right now is the Legendary Golden Sword and the Ectro/Extro Set.

To make lots of money, hunt monsters in Glaphan (levels 99-105). You can earn about 10 million penya per hour. It’s like what you did at level 72 (just farming).

## **Lv 110/111+**

Once you hit level 110 or 111, move to Azria and keep leveling until 120. Then get a Bloody Sword and make it as strong as possible. The price depends on the server.

After level 120, go to Augu(125), then Ghosts(132), and finally Mammoths(13) to keep leveling up. While doing this, improve your gear to the best you can. Focus on making your gear and jewelry stronger.

When you reach level 137, go to Coral Island and fight monsters there. Farm lots of money to improve your gear even more for example at Tiger.


## 
# Pets

I highly advise acquiring a Critical Damage (Crit-DMG) pet once your Critrate surpasses 70%—this is beneficial both for leveling and engaging in CW or Meteor fights, where a Crit-Rate Pet is particularly advantageous.

# Red Meteonyker Video

PURE STR
https://youtu.be/c40GTB_0k-M

PURE STA
https://www.youtube.com/watch?v=ZjeDWQsWhqA

general rule: if you want to solo farm CW/ Red meteor allways go pure sta as of current update you lose less than 1% DPS
super comfort runs, u only die on melts, so just comfortable run away while having 30k+ HP 

pure str suffering more than 10% DPS nerf


# The 1st FWC final meta Build 

This prevailing meta build is contingent on possessing the latest jewelry set, requiring specific components:

FWC (140 Set, FWC 140 1h Weapon) OR 135 Set, Bloody Sword

Champions Set, either at +0 or +4 (important to note FP Necklace)


The rationale behind employing Sword and Shield (SnS) lies in the meticulous consideration of the absolute min-max factor. SnS demonstrates superior DPS compared to a 2h weapon. The disparity arises from the 2h weapon's dependence on the swordcross with a 22% chance to proc, thus diminishing its overall DPS. Despite the raw strength of 2h weapons and str scaling on axe or sword, the reduction caused by the approximately 22% factor underscores its comparative weakness.

The proposed optimal balancing for SnS Knight and 2h Knight suggests that, under a full DPS build, they should achieve comparable DPS. This approach fosters a balanced environment, enabling the community to explore diverse playstyles instead of adhering to a singular meta build.

It is emphasized that employing a 2h weapon as a DPS choice while playing as a knight is deemed counterproductive to the party, given its substantially lower DPS (around 200k) compared to an SnS's 400-500k DPS. This sentiment is familiar to those who have collaborated in FWC.

# Envy Leveling 
-- creating a new one


> **This Guide is for any SNS lover.**

>
>> **This Guide is written in Markdown.**




